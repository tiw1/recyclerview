package com.pakawat.graderecyclerview2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pakawat.graderecyclerview2.model.Grade

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val grades : List<Grade> = listOf(
            Grade("mobile",3.00),
            Grade("kotlin",2.00)
        )


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdpter(grades)
    }

    class ItemAdpter(val grades : List<Grade>) : RecyclerView.Adapter<ItemAdpter.ViewHolder>() {

        //get view ใน item ให้หมด
        class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView){
                val subject = itemView.findViewById<TextView>(R.id.subject)
                val grade = itemView.findViewById<TextView>(R.id.grade)
        }

        // setLayout ของ recycler
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdpter.ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item , parent ,false)
            return ViewHolder(adapterLayout)
        }


         override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                holder.subject.text = grades[position].subject
                holder.grade.text = grades[position].grade.toString()

        }


        override fun getItemCount(): Int {
            return grades.size
        }



    }


}